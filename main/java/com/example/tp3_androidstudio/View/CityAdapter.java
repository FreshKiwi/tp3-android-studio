package com.example.tp3_androidstudio.View;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tp3_androidstudio.Model.City;
import com.example.tp3_androidstudio.R;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.MyViewHolder> {
    private List<City> mDataset;
    private Context context;
    private View.OnClickListener mOnItemClickListener;
    private LayoutInflater mInflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CityAdapter(Context context, List<City> cityList) {
        this.context = context;
        this.mDataset = cityList;
        this.mInflater = LayoutInflater.from(context);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CityAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = mInflater.inflate(R.layout.row, parent, false);
        return new MyViewHolder(v);
    }

    public void setDataset(List<City> dataset){
        this.mDataset = dataset;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        City city = mDataset.get(position);
        holder.cityName.setText(city.getName());
        holder.countryName.setText(city.getCountry());
        holder.temperature.setText(city.getTemperature());


        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            holder.imageWeather.setImageDrawable(context.getResources().getDrawable(context.getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, context.getPackageName())));
            holder.imageWeather.setContentDescription(city.getDescription());
        }


    }


    public City getItem(int id) {
        return mDataset.get(id);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setOnItemClickListener(View.OnClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityName, countryName, temperature;
        ImageView imageWeather;

        public MyViewHolder(View v) {
            super(v);
            v.setTag(this);
            v.setOnClickListener(mOnItemClickListener);
            cityName = itemView.findViewById(R.id.cName);
            countryName = itemView.findViewById(R.id.cCountry);
            temperature = itemView.findViewById(R.id.temperature);
            imageWeather = itemView.findViewById(R.id.imageViewRow);
        }


    }
}
