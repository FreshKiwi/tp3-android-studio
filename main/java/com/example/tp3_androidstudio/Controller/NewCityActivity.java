package com.example.tp3_androidstudio.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tp3_androidstudio.Model.City;
import com.example.tp3_androidstudio.R;

//Error when adding a new city...
public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City city = new City(textName.getText().toString(), textCountry.getText().toString());
                Intent intent = new Intent();
                intent.putExtra("new city", city);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }


}
