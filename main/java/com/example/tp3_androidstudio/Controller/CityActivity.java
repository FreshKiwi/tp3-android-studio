package com.example.tp3_androidstudio.Controller;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tp3_androidstudio.Model.City;
import com.example.tp3_androidstudio.R;
import com.example.tp3_androidstudio.WebServices.JSONResponseHandler;
import com.example.tp3_androidstudio.WebServices.WebServiceUrl;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        city = (City) getIntent().getParcelableExtra("cityToSend");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new WeatherUpdate().execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("updated city", city);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();

    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature()+" °C");
        textHumdity.setText(city.getHumidity()+" %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity()+" %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }


    public class WeatherUpdate extends AsyncTask<Object, Integer, City>{

        @Override
        protected City doInBackground(Object[] objects) {
            HttpURLConnection url = null;
            try {
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                url = (HttpURLConnection) urlToRequest.openConnection();
                url.setRequestMethod("GET");
                url.connect();
                InputStream inputStream = url.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                url.disconnect();
            }
            return city;

        }

        @Override
        protected void onPostExecute(City city) {
            updateView();
            super.onPostExecute(city);

        }
    }
   
}
