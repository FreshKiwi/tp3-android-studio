package com.example.tp3_androidstudio.Controller;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.example.tp3_androidstudio.Model.City;
import com.example.tp3_androidstudio.Model.WeatherDbHelper;
import com.example.tp3_androidstudio.R;
import com.example.tp3_androidstudio.View.CityAdapter;
import com.example.tp3_androidstudio.WebServices.JSONResponseHandler;
import com.example.tp3_androidstudio.WebServices.WebServiceUrl;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final int REQUEST_CODE_NEW_CITY_ACTIVITY = 1;
    private static final int REQUEST_CODE_CITY_ACTIVITY = 2;
    private ListView mListView;
    private WeatherDbHelper weatherDb;
    private Cursor mCursor;
    private SimpleCursorAdapter cursorAdapter;
    private List<City> cities;
    private RecyclerView cityRecyclerView;
    private CityAdapter mAdapter;
    SwipeRefreshLayout swipeLayout;
    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) view.getTag();
            int position = viewHolder.getAdapterPosition();
            City citySelected = cities.get(position);
            Toast.makeText(MainActivity.this, "You Clicked: " + citySelected.getName(), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, CityActivity.class);
            intent.putExtra("cityToSend", cities.get(position));
            startActivityForResult(intent, REQUEST_CODE_CITY_ACTIVITY);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        weatherDb = new WeatherDbHelper(this);
        weatherDb.populate();
        cities = weatherDb.getAllCities();
        cityRecyclerView = (RecyclerView) findViewById(R.id.cityRecycler);
        // use a linear layout manager
        cityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        // specify an adapter
        mAdapter = new CityAdapter(this,cities);
        cityRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(onItemClickListener);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swipeLayout.setOnRefreshListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, REQUEST_CODE_NEW_CITY_ACTIVITY);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (REQUEST_CODE_CITY_ACTIVITY == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra("updated city");
            weatherDb.updateCity(updatedCity);
            cities = weatherDb.getAllCities();
            mAdapter.setDataset(cities);
        }

        if (REQUEST_CODE_NEW_CITY_ACTIVITY == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra("new city");
            weatherDb.addCity(newCity);
            cities = weatherDb.getAllCities();
            mAdapter.setDataset(cities);
        }
    }


    @Override public void onRefresh() {

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                new WeatherUpdates().execute();
                swipeLayout.setRefreshing(false);

            }
        }, 1000);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    class WeatherUpdates extends AsyncTask<Object, Integer, List<City>> {
        private List<City> mCityList;

        @Override
        protected void onPreExecute() {
            // mCityList = dbHelper.getAllCities();
            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city : cities) {
                weatherRequest(city);
                weatherDb.updateCity(city);
            }
            return mCityList;
        }

        protected City weatherRequest(City city) {
            HttpURLConnection url = null;
            try {
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                url = (HttpURLConnection) urlToRequest.openConnection();
                url.setRequestMethod("GET");
                url.connect();
                InputStream inputStream = url.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                url.disconnect();
            }
            return city;

        }

        @Override
        protected void onPostExecute(List<City> cities) {
            cities = weatherDb.getAllCities();
            mAdapter.setDataset(cities);
            swipeLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }
    }

}
